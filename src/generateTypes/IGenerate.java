package generateTypes;

import others.Grain;

public interface IGenerate {
    Grain[][] randomize(Grain[][] grains, int width, int height, int[] parameters);
}

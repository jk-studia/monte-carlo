package neighborhoods;

import others.Grain;

public interface INeighborhood {
    int[][] getNeighbors(Grain grain);
}

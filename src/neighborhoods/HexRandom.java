package neighborhoods;

import others.Grain;

public class HexRandom implements INeighborhood {

    @Override
    public int[][] getNeighbors(Grain grain) {
        int x;
        INeighborhood hexL = new HexL();
        INeighborhood hexR = new HexR();
        int[][] newNeighbors;

        x = (int)(Math.random()*2+1);
        if(x%2==0){
            newNeighbors = hexL.getNeighbors(grain);
        }else {
            newNeighbors = hexR.getNeighbors(grain);
        }
        return newNeighbors;
    }
}

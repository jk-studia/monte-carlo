package controllers;

import boundaryConditions.IBoundary;
import boundaryConditions.NPBC;
import boundaryConditions.PBC;
import generateTypes.IGenerate;
import generateTypes.RandomGenerate;
import generateTypes.RandomRGenerate;
import generateTypes.UniformGenerate;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import neighborhoods.*;
import others.Grain;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.ResourceBundle;

public class ProgramController implements Initializable {

    public TextField xField;
    public TextField yField;
    public CheckBox PBCCheckBox;
    public ScrollPane scrollPane;
    public Button randomizeButton;
    public Button generateButton;
    public Button startButton;
    public Button pauseButton;
    public Button resetButton;
    public Button saveButton;
    public TextField randomTBTextField;
    public TextField equalTBTextField;
    public TextField randomRTBTextField2;
    public TextField randomRTBTextField1;
    public ToggleButton randomToggleButton;
    public ToggleButton randomRToggleButton;
    public ComboBox<String> comboBox;
    public ToggleButton equalToggleButton;
    public TextArea textArea;
    public Button monteCarloButton;
    public Button stopButton;

    private GraphicsContext gc;
    private Canvas canvas;
    private Grain[][] grains;
    private int width = 100;
    private int height = 100;
    private INeighborhood neighborhood;
    private Timeline timeline;
    private double simulationTimeStep = 0.5;
    //==================================== Inicjalizacja wstępna
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        IBoundary boundary = new NPBC();
        createGrains(boundary);

        canvas = new Canvas(width*Grain.SIZE, height*Grain.SIZE);
        canvas.setOnMouseClicked(event->{
            int x = ((int) event.getX())/Grain.SIZE;
            int y = ((int) event.getY())/Grain.SIZE;
            revive(x,y);
        });

        gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.DARKGREY);
        gc.fillRect(0,0,width*Grain.SIZE,height*Grain.SIZE);
        scrollPane.setContent(canvas);

        comboBox.getItems().addAll("1. Moore", "2. Von Neumann", "3. Hex Left", "4. Hex Right", "5. Hex Random", "6. Pentagonal Random");
        randomTBTextField.setDisable(true);
        randomRTBTextField1.setDisable(true);
        randomRTBTextField2.setDisable(true);
        equalTBTextField.setDisable(true);
        textArea.setEditable(false);
    }

    private void revive(int x, int y) {
        if(!grains[x][y].isAlive()) {
            grains[x][y].randomColor();
            grains[x][y].setAlive(true);
            Random random = new Random();
            int tmp = random.nextInt(height*width*100);
            grains[x][y].setID(tmp);
            draw();
        } else {
            grains[x][y].setAlive(false);
            remove(x,y);
        }
    }

    private void draw() {
        for(int i = 0 ; i < width; i++){
            for(int j = 0 ;j < height; j++){
                if(grains[i][j].isAlive()){
                    gc.setFill(grains[i][j].getColor());
                    int x = grains[i][j].getCoordX()*Grain.SIZE;
                    int y = grains[i][j].getCoordY()*Grain.SIZE;
                    gc.fillRect(x,y,Grain.SIZE,Grain.SIZE);
                }
            }
        }
    }

    private void remove(int x, int y) {
        if(x<width&&y<height){
            gc.setFill(Color.DARKGREY);
            gc.fillRect(x*Grain.SIZE, y*Grain.SIZE, Grain.SIZE, Grain.SIZE);
        }
    }

    private void createGrains(IBoundary boundary){
        grains = new Grain[width][height];
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                grains[i][j] = new Grain(i,j);
                grains[i][j].setAlive(false);
                grains[i][j] = boundary.calc(grains[i][j], width, height);
            }
        }
    }

    public void generateButtonOnAction(ActionEvent actionEvent) {
        try {
            width = Integer.parseInt(xField.getText());
            height = Integer.parseInt(yField.getText());
            if(width == 0 || height==0){
                Exception e = new Exception();
                throw e;
            }
            canvas.setWidth(width*Grain.SIZE);
            canvas.setHeight(height*Grain.SIZE);
            gc.setFill(Color.DARKGREY);
            gc.fillRect(0,0,width*Grain.SIZE,height*Grain.SIZE);
            IBoundary boundary;
            if(PBCCheckBox.isSelected()){
                boundary = new PBC();
                textArea.appendText("Wygenerowano planszę "+width+"x"+height+" z periodycznymi warunkami brzegowymi\n");
            }else{
                boundary = new NPBC();
                textArea.appendText("Wygenerowano planszę "+width+"x"+height+" z nieperiodycznymi warunkami brzegowymi\n");
            }
            createGrains(boundary);

        }catch (Exception e){
            textArea.appendText("ERROR: Błąd tworzenia planszy\n");
        }

    }

    public void checkNeighborhood() throws Exception{
        SingleSelectionModel<String> neighborhoodSelectionModel = comboBox.getSelectionModel();
        switch (neighborhoodSelectionModel.getSelectedIndex()) {
                case 0:
                    neighborhood = new Moore();
                    break;
                case 1:
                    neighborhood = new VonNeumann();
                    break;
                case 2:
                    neighborhood = new HexL();
                    break;
                case 3:
                    neighborhood = new HexR();
                    break;
                case 4:
                    neighborhood = new HexRandom();
                    break;
                case 5:
                    neighborhood = new PentagonalRandom();
                    break;
                default: {
                    Exception e = new Exception();
                    throw e;
                }
            }

    }

    public void randomizeButtonOnAction(ActionEvent actionEvent) {
        IGenerate generate;
        int[] parameters = new int[2];
        if(randomToggleButton.isSelected()){
            parameters[0] = Integer.parseInt(randomTBTextField.getText());
            generate = new RandomGenerate();
            grains = generate.randomize(grains, width, height, parameters);
            draw();
        }
        if(equalToggleButton.isSelected()){
            parameters[0] = Integer.parseInt(equalTBTextField.getText());
            generate = new UniformGenerate();
            grains = generate.randomize(grains, width, height, parameters);
            draw();
        }
        if(randomRToggleButton.isSelected()){
            parameters[0] = Integer.parseInt(randomRTBTextField1.getText());
            parameters[1] = Integer.parseInt(randomRTBTextField2.getText());
            generate = new RandomRGenerate();
            grains = generate.randomize(grains, width, height, parameters);
            draw();
        }
    }

    public void startButtonOnAction(ActionEvent actionEvent) {
        try {
            checkNeighborhood();
            KeyFrame simulation = generateKeyFrameSimulation(simulationTimeStep);
            timeline = new Timeline(simulation);
            timeline.setCycleCount(Timeline.INDEFINITE);
            textArea.appendText("Rozpoczęto symulację rozrostu ziaren\n");
            timeline.play();
        }catch (Exception e){
            textArea.appendText("ERROR: Nie wybrano sąsiedztwa\n");
        }
    }

    private KeyFrame generateKeyFrameSimulation(double duration) {
        KeyFrame simulation = new KeyFrame(Duration.seconds(duration),e->{
            simulate();
            draw();
        });
        return simulation;
    }

    private void simulate(){
        Grain[][] tmp = new Grain[width][height];
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                tmp[i][j] = grains[i][j].getCopy();
            }
        }

        int[][] neighbors;
        boolean hasNextStep = false;

        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                if(grains[i][j].isAlive()){
                    neighbors = neighborhood.getNeighbors(grains[i][j]);

                    for(int k = 0; k < neighbors.length; k++){
                        int x = neighbors[k][0];
                        if(x < 0) continue;
                        int y = neighbors[k][1];
                        if(y < 0) continue;

                        if(!tmp[x][y].isAlive()){
                            tmp[x][y].setAlive(true);
                            tmp[x][y].setColor(grains[i][j].getColor());
                            tmp[x][y].setID(grains[i][j].getID());
                            hasNextStep = true;
                        }
                    }
                }
            }
        }
        if(!hasNextStep){
            timeline.stop();
            textArea.appendText("Zakończono symulację\n");
        }
        grains = tmp;
    }

    public void pauseButtonOnAction(ActionEvent actionEvent) {
        if(timeline != null){
            if (timeline.getStatus().equals(Animation.Status.RUNNING)){
                textArea.appendText("Zapauzowano symulację\n");
                timeline.pause();
            }
        }
    }

    public void resetButtonOnAction(ActionEvent actionEvent) {
        if (timeline != null) {
            if (timeline.getStatus().equals(Animation.Status.PAUSED)) {
                timeline.stop();
            } else if (timeline.getStatus().equals(Animation.Status.RUNNING)) {
                timeline.stop();
            }
        }

        IBoundary boundary;
        if(PBCCheckBox.isSelected()){
            boundary = new PBC();
        }
        else{
            boundary = new NPBC();
        }

        grains = new Grain[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                grains[i][j] = new Grain(i, j);
                grains[i][j] = boundary.calc(grains[i][j], width, height);
            }
        }
        gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.DARKGREY);
        gc.fillRect(0,0, width*Grain.SIZE, height*Grain.SIZE);
    }

    public void saveButtonOnAction(ActionEvent actionEvent) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
        String name = simpleDateFormat.format(date);
        File file = new File("result_"+name+".png");
        try {
            WritableImage image = new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
            canvas.snapshot(null, image);
            RenderedImage renderedImage = SwingFXUtils.fromFXImage(image, null);
            ImageIO.write(renderedImage, "png", file);
            textArea.appendText("Zapisano plik pod nazwą: "+"result_"+name+".png");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void randomToggleButtonOnAction(ActionEvent actionEvent) {
        if(randomToggleButton.isSelected()) {
            randomTBTextField.setDisable(false);
            randomRTBTextField1.setDisable(true);
            randomRTBTextField2.setDisable(true);
            equalTBTextField.setDisable(true);
            equalToggleButton.setSelected(false);
            randomRToggleButton.setSelected(false);
        }
        else{
            randomTBTextField.setDisable(true);
        }
    }

    public void equalToggleButtonOnAction(ActionEvent actionEvent) {
        if(equalToggleButton.isSelected()){
            randomTBTextField.setDisable(true);
            randomRTBTextField1.setDisable(true);
            randomRTBTextField2.setDisable(true);
            equalTBTextField.setDisable(false);
            randomToggleButton.setSelected(false);
            randomRToggleButton.setSelected(false);
        }
        else{
            equalTBTextField.setDisable(true);
        }
    }

    public void randomRToggleButtonOnAction(ActionEvent actionEvent) {
        if(randomRToggleButton.isSelected()){
            randomTBTextField.setDisable(true);
            randomRTBTextField1.setDisable(false);
            randomRTBTextField2.setDisable(false);
            equalTBTextField.setDisable(true);
            equalToggleButton.setSelected(false);
            randomToggleButton.setSelected(false);
        }
        else{
            randomRTBTextField1.setDisable(true);
            randomRTBTextField2.setDisable(true);
        }
    }

    public void monteCarloButtonOnAction(ActionEvent actionEvent) {
        KeyFrame simulation = generateMonteCarloSimulation(simulationTimeStep);
        timeline = new Timeline(simulation);
        timeline.setCycleCount(Timeline.INDEFINITE);
        textArea.appendText("Rozpoczęto symulację monte carlo\n");
        timeline.play();
    }

    private KeyFrame generateMonteCarloSimulation(double duration) {
        KeyFrame simulation = new KeyFrame(Duration.seconds(duration),e->{
            simulateMonteCarlo();
            draw();
        });
        return simulation;
    }

    private void simulateMonteCarlo(){
        Grain[][] tmp = new Grain[width][height];
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                tmp[i][j] = grains[i][j].getCopy();
            }
        }

        Random random = new Random();
        for(int index = 0; index < (height*width); index++) {


            int x = random.nextInt(width);
            int y = random.nextInt(height);

            int[][] neighbors = tmp[x][y].getNeighborhood();
            int[][] energy = new int[8][2];
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 2; j++) {
                    energy[i][j] = -1;
                }
            }
            for (int i = 0; i < 8; i++) {
                int tmpX = neighbors[i][0];
                int tmpY = neighbors[i][1];
                if (tmpX > -1 && tmpY > -1) {
                    energy[i][0] = tmp[tmpX][tmpY].getID();
                }
            }
            for (int i = 0; i < 8; i++) {
                if (energy[i][0] != -1) {
                    int tmpE = energy[i][0];
                    int exist = 0;
                    for (int j = 0; j < 8; j++) {
                        if (energy[j][0] == tmpE) {
                            exist++;
                        }
                    }
                    energy[i][1] = exist;
                }
            }
                int max = 0;
                for (int i = 0; i < 7; i++) {
                    if (energy[i][1] < energy[i + 1][1]) {
                        max = i + 1;
                    }
                }

                for (int i = 0; i < 8; i++) {
                    int tmpX = neighbors[i][0];
                    int tmpY = neighbors[i][1];
                    if (tmpX > -1 && tmpY > -1) {
                        if (energy[max][0] == tmp[tmpX][tmpY].getID()) {
                            tmp[x][y].setID(tmp[tmpX][tmpY].getID());
                            tmp[x][y].setColor(tmp[tmpX][tmpY].getColor());
                            break;
                        }
                    }
                }
            }

        grains = tmp;
    }

    public void stopButtonOnAction(ActionEvent actionEvent) {
        if(timeline!=null){
            if (timeline.getStatus().equals(Animation.Status.RUNNING)){
                textArea.appendText("Zakończono symulację\n");
                timeline.stop();
            }
        }
    }
}

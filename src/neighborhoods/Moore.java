package neighborhoods;

import others.Grain;

public class Moore implements INeighborhood {

    @Override
    public int[][] getNeighbors(Grain grain) {
        return grain.getNeighborhood();
    }
}

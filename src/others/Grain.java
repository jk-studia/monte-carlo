package others;

import javafx.scene.paint.Color;


public class Grain {
    //================================ pola
    public static int SIZE = 5;
    private boolean alive;
    private int ID;
    private int coordX, coordY;
    private int[][] neighborhood = new int[8][2];
    private Color color;

    //=============================== metody
    public Grain(){

    }

    public Grain(int coordX, int coordY){
        this.coordX = coordX;
        this.coordY = coordY;
    }

    public void randomColor(){
        if(color == null){
            color = Color.color(Math.random(), Math.random(), Math.random());
        }
    }

    public Grain getCopy(){
        Grain grain = new Grain();
        grain.setAlive(this.alive);
        grain.setCoordX(this.coordX);
        grain.setCoordY(this.coordY);
        grain.setNeighborhood(this.neighborhood);
        grain.setColor(this.color);
        grain.setID(this.ID);
        return grain;
    }

    // ================================== akcesory
    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public int getCoordX() {
        return coordX;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }


    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int[][] getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(int[][] neighborhood) {
        this.neighborhood = neighborhood;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
